#!/bin/bash

# This script is found in the same repository as the docker image, the link
# for which is in the dockerfile. I'm adding comments to enhance my understanding
# of how this script works.
#  basically here we configure the litecoin conf, the values for which are
# documented here: https://litecoin.info/index.php/Litecoin.conf
set -e

if [[ "$1" == "litecoin-cli" || "$1" == "litecoin-tx" || "$1" == "litecoind" || "$1" == "test_litecoin" ]]; then
	mkdir -p "$LITECOIN_DATA"

	cat <<-EOF > "$LITECOIN_DATA/litecoin.conf"
	printtoconsole=1
	rpcallowip=::/0
	${LITECOIN_EXTRA_ARGS}
	EOF
	chown litecoin:litecoin "$LITECOIN_DATA/litecoin.conf"

	# ensure correct ownership and linking of data directory
	# we do not update group ownership here, in case users want to mount
	# a host directory and still retain access to it
	chown -R litecoin "$LITECOIN_DATA"
	ln -sfn "$LITECOIN_DATA" /home/litecoin/.litecoin
	chown -h litecoin:litecoin /home/litecoin/.litecoin

	exec gosu litecoin "$@"
else
	exec "$@"
fi
