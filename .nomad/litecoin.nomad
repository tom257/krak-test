/*
I have not tested the implementation of this nomad file unlike the kubernetes
stateful set which was deployed. This file is largely taken from the
example at https://learn.hashicorp.com/nomad/stateful-workloads/host-volumes#deploy-mysql
and shamelessly adapted for the daemon. This is my weakest of the questions posed.
*/
job "litecoin-daemon" {
  type        = "service"

  group "litecoin-daemon" {
    count = 1

    volume "litecoin" {
      type      = "host"
      read_only = false
      source    = "litecoin"
    }

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    task "litecoin-daemon" {
      driver = "docker"

      volume_mount {
        volume      = "litecoin"
        destination = "/data"
        read_only   = false
      }

      config {
        image = "<DOCKER_IMAGE>"

        port_map {
          lte = 9333
        }
      }

      resources {
        cpu    = 500
        memory = 3000

        network {
          port "lte" {
            static = 9333
          }
        }
      }

      service {
        name = "litecoin-daemon"
        port = "lte"

        check {
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}

