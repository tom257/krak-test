# The following is adapted from:
# https://github.com/NicolasDorier/docker-bitcoin/tree/master/litecoin/0.16.3
# Inline comments explain my understanding of each step of the build and explain
# modifications.

# start with a small image, going forward this image only adds what it needs
# to get the job done, using the bare minumum to keep the attack surface
# of this pod as low as possible.
FROM debian:stretch-slim

# don't run as root. Add a new system (-r) group and user with a home directory
# (-m)
RUN groupadd -r litecoin && useradd -r -m -g litecoin litecoin

# update and install the bare essentials. Setting -e lets this stage fail and
# exit should anything go wrong.
# Do not upgrade the system, this can  cause issues
# (see https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#run)
# this command finishes by removing the apt/lists directory, we do not want
# any futher apt functions to take place in order to keep the container immutable
# and secure, so removing this directory is safe to do and a great idea!
# DEPENDANCIES:
# ca-certificates: Contains the certificate authorities shipped with Mozilla's
# browser to allow SSL-based applications to check for the authenticity of SSL connections.
# dirmng: Dirmngr is a server for managing and downloading certificate revocation lists
# gosu: tool to replace su and sudo, allowing better control of running specific applications
# as specific users. It is used in start up script.
# gpg: public key cryptography.
# wget: used to download binaries.
RUN set -ex \
	&& apt-get update \
	&& apt-get install -qq --no-install-recommends ca-certificates dirmngr gosu gpg wget \
	&& rm -rf /var/lib/apt/lists/*

# The next layer of this image sets up environment varialbles for downloading
# and validating the correct version of litecoin.
ENV LITECOIN_VERSION 0.17.1
ENV LITECOIN_URL https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz
ENV LITECOIN_SHA256 9cab11ba75ea4fb64474d4fea5c5b6851f9a25fe9b1d4f7fc9c12b9f190fed07
ENV LITECOIN_ASC_URL https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-linux-signatures.asc
ENV LITECOIN_PGP_KEY FE3348877809386C

# Again, setting -e ensures should anything go wrong validating the downloads.
# Install litecoin binaries and carryout validation on the downloaded files and
# keys using sha256 to validate the binary has not been tampered with and futher
# check the signature file for this binary can be properly verified gpg and as
# a final step cleaning up the unused files.
RUN set -ex \
	&& cd /tmp \
	&& wget -qO litecoin.tar.gz "$LITECOIN_URL" \
	&& echo "$LITECOIN_SHA256 litecoin.tar.gz" | sha256sum -c - \
	&& gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$LITECOIN_PGP_KEY" \
	&& wget -qO litecoin.asc "$LITECOIN_ASC_URL" \
	&& gpg --verify litecoin.asc \
	&& tar -xzvf litecoin.tar.gz -C /usr/local --strip-components=1 --exclude=*-qt \
	&& rm -rf /tmp/*

# create data directory and ensure correct ownership. Also create a link
# from the data diretory to the default location: https://litecoin.info/index.php/Data_directory
ENV LITECOIN_DATA /data
RUN mkdir "$LITECOIN_DATA" \
	&& chown -R litecoin:litecoin "$LITECOIN_DATA" \
	&& ln -sfn "$LITECOIN_DATA" /home/litecoin/.litecoin \
	&& chown -h litecoin:litecoin /home/litecoin/.litecoin
VOLUME /data
# entrypoint sets up the conf for litecoin, I have added some comments in that
# file.
COPY docker-entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
EXPOSE 9332
CMD ["litecoind"]
