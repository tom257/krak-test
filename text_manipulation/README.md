# Text Manipulation

A self-invented problem: The command kubectl get secret <MY_SECRET> -o yaml returns a yaml document containing base64 encoded secret values. I would like a simple way to list just the secrets returned by this command in plain text. Whilst there are better ways to achieve this result (such as using jq or yq)  the below example uses sed and awk to output the key and plain text secret.

After piping the output of kubectl into sed, we extract only the values that using sed's address ranges, using a futher sed script {} to exclude the enclosing address values. Awk is then used to split the line on the ': ' seperator, decode the base64 of the second value and then reassemble in a clean, readable output.  The awk part of this solution as taken from: https://stackoverflow.com/questions/27323531/base64-data-in-awk

```shell
kubectl get secret <MY_SECRET> -o yaml | sed -n '/^data:/,/^[a-z]/ {
    /^data:/n
    /^[a-z]/ !p
}' | awk -F ': ' '{
    "echo " $2 "| base64 -d" | getline x
    print $1, x
}'
```

### output:
```shell
username admin
password 1f2d1e2e67df
```

`main.go` in this directory contains a solution to the same problem in go.
