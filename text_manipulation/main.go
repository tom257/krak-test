package main

import (
	"encoding/base64"
	"fmt"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

const usage = "Usage: kubectl get secret <MY_SECRET> -o yaml | decoder "

func main() {
	// open the file pointing to stdin
	f := os.Stdin

	info, err := f.Stat()
	if err != nil {
		log.Fatalf("could not open stdin file descriptor: %v", err)
	}

	// check the pipe contains something and print a useful message if it does
	// not and exit.
	if info.Size() <= 0 {
		fmt.Println(usage)
		os.Exit(1)
	}

	// We will read the whole file into memory, this comes with certain risks,
	// but in this case I feel like this solution is acceptable.
	buf := make([]byte, info.Size())

	_, err = f.Read(buf)
	if err != nil {
		log.Fatalf("could not read from stdin: %v", err)
	}

	secrets, err := parseAndDecode(buf)
	if err != nil {
		log.Fatalf("could not parse kubectl output: %v\n\n%s", err, usage)
	}

	// print the secrets to stdout nicely
	for k, v := range secrets {
		fmt.Printf("%v: %v\n", k, v)
	}
}

// parseAndDecode attempts to parse a yaml file from a byte slice and decode
// any base64 encoded secrets, returning any decoded values as map.
func parseAndDecode(b []byte) (map[string]string, error) {
	// we only need the data field:
	t := struct {
		Data map[string]string `yaml:"data"`
	}{}

	if err := yaml.Unmarshal(b, &t); err != nil {
		return nil, err
	}

	// we could check length and return an error if there are no secrets, but
	// I don't think that is an error of the program so in the intrerest of
	// simpliciy we continue.
	for k, v := range t.Data {
		decoded, err := base64.StdEncoding.DecodeString(v)
		if err != nil {
			fmt.Printf("decoding: can't decode secret with key: %v", err)
			continue
		}

		// override the origional map value:
		t.Data[k] = string(decoded)
	}

	return t.Data, nil
}
